<?php get_header(); ?>

<div class="filter">
    <form class="filter-form" action="">
      <?php
      $terms = get_terms(array(
        'taxonomy' => 'department-taxonomy',
        'hide_empty' => 0,
        'orderby' => 'term_order'
      ));
      ?>
        <div class="dropdown-wrapper large-6 medium-6 small-12 columns" id="sort-department-wrapper">

          <?php
          // Grab the fpd from the request.
          $fpd = $_GET['fpd'];
          ?>

            <label for="fpd">Choose a Department, Program, or Office:</label>
            <select name="fpd" id="fpd">
                <option value="">Select one</option>

              <?php foreach ($terms as $term) {
                $class = 'parent';

                $parent = get_term_meta($term->term_id, 'is_parent', true);

                if (!$parent) {
                  $class = 'child';
                } ?>

                  <option value="<?php echo $term->slug; ?>"
                          class="<?php echo $class; ?>" <?php echo $fpd == $term->slug ? 'selected' : '' ?>>
                    <?php echo $term->name ?>
                  </option>
              <?php } ?>
            </select>
        </div>

        <div id="search-name" class="large-6 medium-6 small-12 columns">
            <label for="search">Search for a person's first and/or last name:</label>
            <div class="search-input">
                <input type="search" class="search-field" placeholder="Enter name"
                       value="<?= esc_attr($_GET['fpk']); ?>" name="s" title="Search for:" id="search">
                <button type="submit"><i class="fa fa-search"></i><span class="screen-reader-text">Search</span>
                </button>
            </div>
        </div>
    </form>
</div>


<?php
wp_reset_query();
?>


<? /* Added 1/30/2018 */ ?>
<div class="content-section large-12 columns" id="faculty-profiles">


  <?
  // FACULTY PROFILES LISTING
  $args = array(
    'post_type' => 'facultyprofile',
    'posts_per_page' => -1,
    'meta_key' => 'faculty_last_name',
    'orderby' => 'meta_value',
    'order' => 'ASC',
  );

  if ($_GET['fpd']) {
    $args['tax_query'] = array(
      array(
        'taxonomy' => 'department-taxonomy',
        'field' => 'slug',
        'terms' => array($_GET['fpd'])
      )
    );
    /*cmb*/
    if ($_GET['fpd'] == 'all') unset($args['tax_query']);
    /*cmb*/
  }

  if ($_GET['fpk']) {
    $args['s'] = $_GET['fpk'];
  }

  /*cmb */
  if ($_GET['fpd'] || $_GET['fpk']) : /*cmb*/

    $the_query = new WP_Query($args);
    ?>

    <? if ($the_query->have_posts()): ?>

    <? // Content ?>
      <ul id="faculty-profiles-list">
        <? while ($the_query->have_posts()): $the_query->the_post(); ?>
            <li class="faculty-profiles-listing">
              <?php //cmb only print a link if there is bio text
              $print_link = FALSE;
              if (@array_pop(get_post_meta(get_the_ID(), 'faculty_bio_introduction'))) {
              $print_link = TRUE; ?>
                <a href="<?php the_permalink(); ?>">
                  <?php } ?>
                    <div class="faculty-profiles-container">
                        <div class="faculty-profile-info">
                          <? if (has_post_thumbnail()): ?>
                              <div class="featured-image">
                                <? the_post_thumbnail('faculty-thumbnail'); ?>
                              </div>
                          <? endif; ?>
                            <div class="information">
                                <h4><? the_title(); ?></h4>
                                <!--<?php if ($meta = @array_pop(get_post_meta(get_the_ID(), 'faculty_short_title'))): ?>
                                            <span><?php echo $meta; ?></span>
                                        <?php elseif ($meta = @array_pop(get_post_meta(get_the_ID(), 'faculty_title'))): ?>
                                            <span><?php echo $meta; ?></span>
                                        <?php endif; ?>-->
                                <span><?php echo @array_pop(get_post_meta(get_the_ID(), 'faculty_title')) ?></span>
                            </div>
                        </div>
                      <? // <hr> ?>
                        <ul class="faculty-profiles-contact">
                          <?
                          $facultyContact = array(
                            'email' => 'envelope-o',
                            'phone' => 'phone',
                            'office' => 'map-marker'
                          );

                          foreach ($facultyContact as $type => $icon) {
                            ?>
                            <?php if ($meta = @array_pop(get_post_meta(get_the_ID(), 'faculty_' . $type . ''))): ?>
                                  <li><i class="fa fa-<? echo $icon ?>"></i><?php echo $meta; ?></li>
                            <?php endif; ?>
                          <? } ?>
                        </ul>
                      <? // <hr> ?>
                      <? // <i class="fa fa-angle-right"></i> ?>
                    </div>
                  <?php if ($print_link) { ?></a> <?php } ?>
            </li>
        <? endwhile; ?>
      </ul>
  <?php else : ?>

    <?php get_template_part('includes/no-results'); ?>

  <? endif; ?>
  <? endif; ?>

</div>

<?php get_footer(); ?>
