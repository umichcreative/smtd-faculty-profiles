(function($){

    // Reloads page with appropriate filters.
    function facultyReload(department, search) {
        window.location = window.location.pathname
            + '?fpd=' + department
            + '&fpk=' + search;
    }

    $(document).ready(function(){
        if( !$('body').hasClass('post-type-archive-facultyprofile') ) {
            return;
        }

        var fpd = $('#fpd');

        // Add hyphens to all the child list options.
        fpd.find('.child').each(function() {
           $(this).text('-' + $(this).text());
        });

        // Handle fpd select list changes.
        fpd.on('change', function() {
           facultyReload($(this).val(), $('#search').val());
        });

        // Handle form submit.
        $('.filter-form').on('submit', function(event) {
           event.preventDefault();
           facultyReload(fpd.val(), $('#search').val());
        });

        /** CARD DYNAMIC DISPLAY **/

        var cardsWrap     = $('#faculty-profiles-list');
        var cards         = cardsWrap.find('> li');
        var bottomPos     = $(window).scrollTop() + $(window).height();
        var cardVisOffset = 120;
        var cardFadeSpeed = 750; // in milliseconds

        /* TEST FILLER CODE */

        if( !window.location.search && (window.location.hostname == 'smtd.umichcreative.org') ) {
            var cardSet = cards.clone();
            var addSets = Math.ceil( 300 / cards.length );

            for( i=0; i <= addSets; i++ ) {
                cardsWrap.append( cardSet.clone() );
            }
            cards = cardsWrap.find('> li');
        }
        /* END TEST FILLER CODE */

        // stupid simple reverse plugin for next block of code
        jQuery.fn.reverse = [].reverse;

        // hide cards not above bottom of window
        // do this in reverse as we need to record the topPos of
        // each element else it will be off
        /*cards.reverse().each(function(){
            if( ($(this).position().top + cardVisOffset) > bottomPos ) {
                $(this).data('topPos', $(this).position().top);
                $(this).addClass('hidden');
            }
        });

        // check to see if hidden items should now be shown.
        $(window).scroll(function(){
            bottomPos = $(window).scrollTop() + $(window).height();
            cards.filter('.hidden').each(function(){
                if( ($(this).data('topPos') + cardVisOffset) <= bottomPos ) {
                    $(this).removeClass('hidden').hide().fadeIn( cardFadeSpeed );
                }
            });
        });*/

        $(window).resize(function(){
            $(window).trigger('scroll');
        });
    });

}(jQuery));
